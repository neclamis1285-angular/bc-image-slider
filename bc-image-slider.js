(function () {
	angular.module('bcImageSlider')
		.directive('bcImageSlider', function ($timeout) {
			return {
				restrict: 'AE',
				replace: true,
				scope: {
					images: '=',
					sliderOptions: '=',
					navArrowColor: '=',
					navArrowBackgroundColor: '='
				},
				link: function (scope, elem, attrs) {

					// Set Initial Index
					scope.currentIndex = 0;

					// Go To Next Slide
					scope.next = function () {
						if (scope.images) {
							scope.currentIndex < scope.images.length - 1 ? scope.currentIndex++ :
								scope.currentIndex = 0;
						}
					};

					// Go To Previous Slide
					scope.prev = function () {
						if (scope.images) {
							scope.currentIndex > 0 ? scope.currentIndex-- : scope.currentIndex =
								scope.images.length - 1;
						}
					};

					// Start Auto Play Of Slideshow
					if (scope.sliderOptions.autoPlay == true) {
						var timer;

						var sliderFunc = function () {
							timer = $timeout(function () {
								scope.next();
								timer = $timeout(sliderFunc, scope.sliderOptions.imageInterval ||
									5000);
							}, scope.sliderOptions.imageInterval || 5000);
						};

						sliderFunc();

						scope.$on('$destroy', function () {
							$timeout.cancel(timer);
						});
					}

					scope.hasImage = function (image) {
						return image && image.src != null;
					};

				},
				template: '<div class="bc-image-slider"><div class="image-slide" ng-repeat="image in images | filter:hasImage" ng-show="$index == currentIndex" ng-style="{\'background-image\': \'url(\' + image.src + \')\'}"></div>' +
				'<a class="left-arrow" ng-href ng-click="prev()"><i class="material-icons" ng-style="{\'background-color\': navArrowBackgroundColor, \'color\':' +
				' navArrowColor}">chevron_left</i></a>' +
				'<a class="right-arrow" ng-href ng-click="next()"><i class="material-icons" ng-style="{\'background-color\': navArrowBackgroundColor, \'color\': navArrowColor}">chevron_right</i></a></div>'
			}
		});
})(angular.module("bcImageSlider", []));
